<html>
<head>
<body>




<h4>
    <center style="color:red;">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
    </center>
</h4>


<form action="/order" method="post">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div class="container-fluid">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null padd_top_30">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <div class="sizes"><strong>Product name</strong></div>
                    <fieldset class="{{ $errors->has('name') ? ' has-error' : '' }}">
                        <input type="text" id="name" name="name"
                               placeholder="Enter the  Phone no....." value="{{ old('name') }}">
                        @if ($errors->has('name'))<span
                                class="help-block error_font"><strong>{{ $errors->first('name') }}</strong></span>@endif
                    </fieldset>


                    <div class="sizes"><strong>price</strong></div>
                    <fieldset class="{{ $errors->has('price') ? ' has-error' : '' }}">
                        <input type="text" id="price" name="price"
                               placeholder="Enter tprice....." value="{{ old('price') }}">
                        @if ($errors->has('price'))<span
                                class="help-block error_font"><strong>{{ $errors->first('price') }}</strong></span>@endif
                    </fieldset>

                    <div class="sizes"><strong>date</strong></div>
                    <fieldset class="{{ $errors->has('date') ? ' has-error' : '' }}">
                        <input type="date" id="date" name="date"
                               placeholder="Enter the  Phone no....." value="{{ old('date') }}">
                        @if ($errors->has('name'))<span
                                class="help-block error_font"><strong>{{ $errors->first('name') }}</strong></span>@endif
                    </fieldset>

                    <div class="sizes"><strong>time</strong></div>
                    <fieldset class="{{ $errors->has('time') ? ' has-error' : '' }}">
                        <input type="text" id="time" name="time"
                               placeholder="Enter time....." value="{{ old('time') }}">
                        @if ($errors->has('time'))<span
                                class="help-block error_font"><strong>{{ $errors->first('time') }}</strong></span>@endif

                        <h5>
                            <center style="color:red;">
                                @if(session()->has('msg'))
                                    <div class="alert alert-success">
                                        {{ session()->get('msg') }}
                                    </div>
                                @endif
                            </center>
                        </h5>
                    </fieldset>




                </div>

            </div>

            <div class="button_submits col-md-12 col-sm-12 col-xs-12 col-lg-12 padd_top_10">
                <input type="submit" value="Submit">&nbsp;
                <input type="reset" value="Clear"></div>

        </div>

    </div>
    </div>
</form>




<div class="container-fluid">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container table_size">
                    <table class="table">
                        <thead>
                        <tr class="boldtable" bgcolor="#d3d3d3">
                            <td>id</td>
                            <td>Product name </td>
                            <td>date </td>
                            <td>time </td>
                            <td>price </td>

                        </tr>
                        </thead>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->date }}</td>
                                <td>{{ $user->time }}</td>
                                <td>{{ $user->price }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</head>
</html>