<html>
<style>
    .title{
        text-align: center;
        font-size: 30px;
        text-shadow: 1px 0;
        color: #000000;
        padding-bottom: 5px;
    }
    .subtitle{
        text-align: center;
        font-size: 16px;
        color: #000000;
    }
    table{
        width:100%;
    }
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    th, td {
        padding: 5px;
        text-align: left;
    }
    .invoice{
        text-align: center;
        font-size: 18px;
        text-shadow: 1px 0;
        color: white;
        background-color:#777777;
    }

    .subtitles{
        text-align: left;
        font-size: 14px;
        color: #000000;
    }
    .topheader{
        padding-top: 10px;
    }

</style>


<body>

<div class="container-fluid profile ">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
            </div>

            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title">TNT Parcel Service</div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 subtitle">No 236c, Newpet, Near Daily Thanthi Office, Roundana Krishnagiri.</div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 subtitle">Krishnagiri - 635 001, Tamil Nadu, S.India</div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 subtitle padd_buttom_10">Phone :123 4567 890 Email:gmail.com</div>

                <table class="topheader">
                    <tr>
                        <td><strong>Serial No: {{$data['id']}}</strong></td>
                        <th class="invoice">Invoice</th>
                        <td><strong>Booking Date: {{$data['invoicedob']}}</strong></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td><strong>Consigner (From)</strong><br>
                            Consigner Phone NO: <strong> {{$data['mobileno']}}</strong><br>
                            Consigner Name: <strong> {{$data['consignername']}}</strong></td>

                        <td><strong>Consigne (To)</strong><br>
                            Consigne Phone NO: <strong>  {{$data['tomobileno']}}</strong><br>
                            Consigne Name: <strong> {{$data['congname']}}</strong></td>
                    </tr>
                    <tr>
                        <td>Destination: </td>
                        <td><strong>  {{$data['destination']}}</strong></td>
                    </tr>
                    <tr>
                        <td>Branch: </td>
                        <td><strong>  {{$data['branch']}}</strong></td>
                    </tr>
                    <tr>
                        <td>Weight: </td>
                        <td><strong>  {{$data['weight']}}</strong></td>
                    </tr>
                    <tr>
                        <td>No of Pkgs: </td>
                        <td><strong>  {{$data['pkgs']}}</strong></td>
                    </tr>
                    <tr>
                        <td>Type of Packing: </td>
                        <td><strong>  {{$data['typeofpacking']}}</strong></td>
                    </tr>
                    <tr>
                        <td>Amount: </td>
                        <td><strong>  {{$data['amount']}}</strong></td>
                    </tr><tr>
                        <td>Freight: </td>
                        <td><strong>  {{$data['freight']}}</strong></td>
                    </tr>
                    <tr>
                        <td><strong>Total:</strong> </td>
                        <td><strong>  {{$data['total']}}</strong></td>
                    </tr>
                    <tr>
                        <td><strong>GrandTotal:</strong> </td>
                        <td><strong>  {{$data['grandtotal']}}</strong></td>
                    </tr>

                </table>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <strong class="subtitles col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_30">Read and Accepted</strong><br>
                        <strong class="subtitles col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_30"> Issue date:
                            <?php echo date("d/m/y");?></strong>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <strong class="subtitles col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_two_em">Signature</strong><br>
                        <strong class="subtitles col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_two_em">..........................................</strong><br>
                    </div>
                </div>

            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
            </div>
        </div>

    </div>
</div>
</body>
</html>
