<html>
<head>
    <body>




    <h4>
        <center style="color:red;">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </center>
    </h4>


    <form action="/one" method="post">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="container-fluid">
            <div class="container">
                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null padd_top_30">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <div class="sizes"><strong>name</strong></div>
                        <fieldset class="{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" id="name" name="name"
                                   placeholder="Enter the  Phone no....." value="{{ old('name') }}">
                            @if ($errors->has('name'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('name') }}</strong></span>@endif
                        </fieldset>

                        <div class="sizes"><strong>email</strong></div>
                        <fieldset class="{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="text" id="email" name="email"
                                   placeholder="Enter the  Phone no....." value="{{ old('email') }}">
                            @if ($errors->has('email'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('email') }}</strong></span>@endif
                        </fieldset>
                        <div class="sizes"><strong>address</strong></div>
                        <fieldset class="{{ $errors->has('address') ? ' has-error' : '' }}">
                            <input type="text" id="address" name="address"
                                   placeholder="Enter the  Phone no....." value="{{ old('address') }}">
                            @if ($errors->has('address'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('address') }}</strong></span>@endif
                        </fieldset>
                        <div class="sizes"><strong>date</strong></div>
                        <fieldset class="{{ $errors->has('date') ? ' has-error' : '' }}">
                            <input type="date" id="date" name="date"
                                   placeholder="Enter the  Phone no....." value="{{ old('date') }}">
                            @if ($errors->has('name'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('name') }}</strong></span>@endif
                        </fieldset> <div class="sizes"><strong>enqire</strong></div>
                        <fieldset class="{{ $errors->has('enq') ? ' has-error' : '' }}">
                            <input type="text" id="enq" name="enq"
                                   placeholder="Enter the  Phone no....." value="{{ old('enq') }}">
                            @if ($errors->has('enq'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('enq') }}</strong></span>@endif
                        </fieldset>

                    </div>

                </div>


                <a class="button_submits col-md-12 col-sm-12 col-xs-12 col-lg-12 padd_top_10">
                    <input type="submit" value="Submit">&nbsp;
                    <input type="reset" value="Clear">
                    <a href="enq">Enq</a>
                </div>

            </div>
        </div>
    </form>




    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="container table_size">
                        <table class="table">
                            <thead>
                            <tr class="boldtable" bgcolor="#d3d3d3">
                                <td>id</td>
                                <td>name </td>
                                <td>email </td>
                                <td>address </td>
                                <td>date </td>
                                <td>enq </td>

                            </tr>
                            </thead>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->address }}</td>
                                    <td>{{ $user->date }}</td>
                                    <td>{{ $user->enq }}</td>
                                    {{--<td><a href = 'seleced/{{ $user->id }}'button class="button button5">Purchase</a></td>--}}
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>
</head>
</html>