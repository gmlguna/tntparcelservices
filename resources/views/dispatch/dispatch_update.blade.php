@extends('layout.main')
@section('title', 'Office Center in Krishnagiri')
@section('keywords', 'Office Center in Krishnagiri')
@section('description', 'Office Center in Krishnagiri')
@section('content')

    <h2 class="student_subhead">
        <div class="paid_backcolor"><strong class="welcomekbas"> </strong>
            <ul class="nav navbar-nav welcomekbas">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbas">Operations<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/paidbooking') }}"> Booking<i
                                        class="icon-arrow-right"></i></a>
                        </li>
                        <li>
                            <a href="{{ url('/serial') }}">Dispatch</a>
                        </li>
                        <li>
                            <a href="{{ url('/serial') }}">Receiver</a>
                        </li>
                        <li>
                            <a href="{{ url('/topaybookingdetails') }}">To- Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                        <li>
                            <a href="{{ url('/manualdetails') }}">Manual Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbass">Search<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/user') }}">Serial No</a>
                        </li>
                        <li>
                            <a href="{{ url('/name') }}">Packing Name</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbass">Dispatch<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/serial') }}">Dispatch</a>
                        </li>
                        <li>
                            <a href="{{ url('/dispatch/dispatch_delete') }}">Delivery Details</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/receiver/receiver_delete') }}" class="welcomekbass">Receiver details</a>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/report') }}" class="welcomekbass">Report</a>
                </li>
            </ul>

            <div class="right_logout">
                <a class="logout" href="{{ url('/paidbooking') }}">PAID</a>
                <a class="logout" href="{{ url('/topaybooking') }}">TO PAY</a>
                <a class="logout" href="{{ url('/manualbooking') }}">MANUAL</a>
                <a class="logout" href="login"><strong>Logout</strong></a>
            </div>
        </div>
    </h2>

    <h4>
        <center style="color:red;">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </center>
    </h4>



    <form action="{{ url('/dispatch/edit/'.$users[0]->id) }}" method="post">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <table  border ="8">

            <tr class="boldtable" bgcolor="#d3d3d3">
                <th>Serial No</th>

                <td>
                    <input type='text' name='id'
                           value='<?php echo $users[0]->id; ?>'/>
                </td>
            </tr>
            <tr class="boldtable" bgcolor="#d3d3d3">
                <th>Dispatch Ledger No</th>

                <td>
                    <input type='text' name='dispatchno'
                           value='<?php echo $users[0]->dispatchno; ?>'/>
                </td>
            </tr>

            <tr class="boldtable" bgcolor="#d3d3d3">
                <th>Vehicle No:</th>

                <td>
                    <input type='text' name='vehicleno'
                           value='<?php echo $users[0]->vehicleno; ?>'/>
                </td>
            </tr>
            <tr class="boldtable" bgcolor="#d3d3d3">
                <th>Driver Name </th>

                <td>
                    <input type='text' name='drivername'
                           value='<?php echo $users[0]->drivername; ?>'/>
                </td>
            </tr>

            <tr class="boldtable" bgcolor="#d3d3d3">
                <th>Cleaner Name</th>

                <td>
                    <input type='text' name='cleanername'
                           value='<?php echo $users[0]->cleanername; ?>'/>
                </td>
            </tr>
            <tr class="boldtable" bgcolor="#d3d3d3">
                <th>Dispatch Date</th>

                <td>
                    <input type='date' name='dispatchdate'
                           value='<?php echo $users[0]->dispatchdate; ?>'/>
                </td>
            </tr>

            <tr class="boldtable" bgcolor="#d3d3d3">
                <th>Destination</th>

                <td>
                    <input type='text' name='destination'
                           value='<?php echo $users[0]->destination; ?>'/>
                </td>
            </tr>

            <tr bgcolor="#d3d3d3">
                <th></th>

                <td>
                    <input type='submit' value="Update "/>
                </td>
            </tr>




        </table>
    </form>



@endsection




