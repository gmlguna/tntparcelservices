@extends('layout.main')
@section('title', 'Office Center in Krishnagiri')
@section('keywords', 'Office Center in Krishnagiri')
@section('description', 'Office Center in Krishnagiri')
@section('content')

    <h2 class="student_subhead">
        <div class="paid_backcolor"><strong class="welcomekbas"> </strong>
            <ul class="nav navbar-nav welcomekbas">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbas">Operations<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/paidbooking') }}"> Booking<i
                                        class="icon-arrow-right"></i></a>
                        </li>
                        <li>
                            <a href="{{ url('/serial') }}">Dispatch</a>
                        </li>
                        <li>
                            <a href="{{ url('/serial') }}">Receiver</a>
                        </li>
                        <li>
                            <a href="{{ url('/topaybookingdetails') }}">To- Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                        <li>
                            <a href="{{ url('/manualdetails') }}">Manual Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbass">Search<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/user') }}">Serial No</a>
                        </li>
                        <li>
                            <a href="{{ url('/name') }}">Packing Name</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbass">Dispatch<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/serial') }}">Dispatch</a>
                        </li>
                        <li>
                            <a href="{{ url('/dispatch/dispatch_delete') }}">Delivery Details</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/receiver/receiver_delete') }}" class="welcomekbass">Receiver details</a>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/report') }}" class="welcomekbass">Report</a>
                </li>
            </ul>

            <div class="right_logout">
                <a class="logout" href="{{ url('/paidbooking') }}">PAID</a>
                <a class="logout" href="{{ url('/topaybooking') }}">TO PAY</a>
                <a class="logout" href="{{ url('/manualbooking') }}">MANUAL</a>
                <a class="logout" href="login"><strong>Logout</strong></a>
            </div>
        </div>
    </h2>
    <h4>
        <center style="color:red;">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </center>
    </h4>


    <form action="/manualbooking" method="post">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

    <div class="container-fluid">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">

                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null padd_buttom_10">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 ">
                        <div class="sizes"><strong>Paid:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('paid') ? ' has-error' : '' }}">
                            <select id="paid" name="paid">
                                <option value="">WayBill Type</option>
                                <option value="Paid">Paid</option>
                                <option value="toPay">To Pay</option>
                                <option value="credit">Credit</option>
                                <option value="foc">FOC</option>
                            </select>
                            @if ($errors->has('paid'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('paid') }}</strong></span>@endif
                        </fieldset>

                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 ">
                        <div class="sizes"><strong>NO Document:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('nodocument') ? ' has-error' : '' }}">
                            <select id="nodocument" name="nodocument">
                                <option value=""> No document</option>
                                <option value="document">Document</option>
                            </select>
                            @if ($errors->has('nodocument'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('nodocument') }}</strong></span>@endif
                        </fieldset>
                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 ">
                        <div class="sizes"><strong>Source City:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('sourcecity') ? ' has-error' : '' }}">
                            <select id="sourcecity" name="sourcecity">
                                <option value="0">-Source City-</option>
                                <option value="Bangalore">Bangalore</option>
                                <option value="Bhavani">Bhavani</option>
                                <option value="Coimbatore">Coimbatore</option>
                                <option value="Destination City">Destination City</option>
                                <option value="Dindigul">Dindigul</option>
                                <option value="Ellampillai">Ellampillai</option>
                                <option value="Erode">Erode</option>
                                <option value="Hosur">Hosur</option>
                                <option value="Jalakandapuram">Jalakandapuram</option>
                                <option value="Mecheri">Mecheri</option>
                                <option value="Omalur">Omalur</option>
                                <option value="Ooty">Ooty</option>
                                <option value="Puliyampatti">Puliyampatti</option>
                                <option value="Salem">Salem</option>
                                <option value="Sathyamangalam">Sathyamangalam</option>
                                <option value="Vellore">Vellore</option>
                            </select>
                            @if ($errors->has('sourcecity'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('sourcecity') }}</strong></span>@endif
                        </fieldset>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 ">
                        <div class="sizes"><strong>Source Branch:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('sourcebranch') ? ' has-error' : '' }}">
                            <select id="sourcebranch" name="sourcebranch">
                                <option value="sourcebranch">Source Branch</option>
                            </select>
                            @if ($errors->has('sourcebranch'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('sourcebranch') }}</strong></span>@endif
                        </fieldset>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 ">
                        <div class="sizes"><strong>Destination City:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('destinationcity') ? ' has-error' : '' }}">
                            <select id="destinationcity" name="destinationcity">
                                <option value="">Select the City</option>
                                <option value="Bangalore">Bangalore</option>
                                <option value="Bhavani">Bhavani</option>
                                <option value="Coimbatore">Coimbatore</option>
                                <option value="Destination City">Destination City</option>
                                <option value="Dindigul">Dindigul</option>
                                <option value="Ellampillai">Ellampillai</option>
                                <option value="Erode">Erode</option>
                                <option value="Hosur">Hosur</option>
                                <option value="Jalakandapuram">Jalakandapuram</option>
                                <option value="Mecheri">Mecheri</option>
                                <option value="Omalur">Omalur</option>
                                <option value="Ooty">Ooty</option>
                                <option value="Puliyampatti">Puliyampatti</option>
                                <option value="Salem">Salem</option>
                                <option value="Sathyamangalam">Sathyamangalam</option>
                                <option value="Vellore">Vellore</option>
                            </select>
                            @if ($errors->has('destinationcity'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('destinationcity') }}</strong></span>@endif
                        </fieldset>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 ">
                        <div class="sizes"><strong>Select Branch:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('selectbranch') ? ' has-error' : '' }}">
                            <select id="selectbranch" name="selectbranch">
                                <option value="sourcebranch">Select Branch</option>
                            </select>
                            @if ($errors->has('selectbranch'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('selectbranch') }}</strong></span>@endif
                        </fieldset>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                    <h4 class="student_subhead"> <div class="paid_backcolors consignemt"> Way Bill Details </div></h4>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null padd_buttom_10">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">

                        <div class="sizes"><strong>Way bill no:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('waybillno') ? ' has-error' : '' }}">
                            <input type="text" id="waybillno" name="waybillno" placeholder="Enter the Way bill no....">
                            @if ($errors->has('waybillno'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('waybillno') }}</strong></span>@endif

                        </fieldset>

                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="sizes"><strong>Way bill Date:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('waybilldate') ? ' has-error' : '' }}">
                            <input type="date" id="waybilldate" name="waybilldate" placeholder="Enter the Way bill date....">
                            @if ($errors->has('waybilldate'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('waybilldate') }}</strong></span>@endif

                        </fieldset>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">

                        <div class="sizes"><strong> Consigner (From)</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('mobileno') ? ' has-error' : '' }}">
                            <input type="text" id="mobileno" name="mobileno"
                                   placeholder="Enter the  Phone no.....">
                            @if ($errors->has('mobileno'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('mobileno') }}</strong></span>@endif
                        </fieldset>

                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null padd_top_10 padd_buttom_20">

                            <div id="flips"> ADD Register</div>
                            <div id="panels">
                                <div class="sizes"><strong>Address:</strong><span class="star-rating">*</span>
                                </div>
                                <fieldset class="{{ $errors->has('address') ? ' has-error' : '' }}">
                                <textarea id="address" name="address" placeholder="Write  address...."
                                          style="height:80px"></textarea>
                                    @if ($errors->has('address'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('address') }}</strong></span>@endif
                                </fieldset>


                                <div class="sizes"><strong>PIN No:</strong><span class="star-rating">*</span></div>
                                <fieldset class="{{ $errors->has('pin') ? ' has-error' : '' }}">
                                    <input type="text" id="pin" name="pin" placeholder="Enter the  Pin no.....">
                                    @if ($errors->has('pin'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('pin') }}</strong></span>@endif
                                </fieldset>

                                <div class="sizes"><strong>Contact Name:</strong><span class="star-rating">*</span>
                                </div>
                                <fieldset class="{{ $errors->has('contactname') ? ' has-error' : '' }}">
                                    <input type="text" id="contactname" name="contactname"
                                           placeholder="Enter the Contact Name...">
                                    @if ($errors->has('contactname'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('contactname') }}</strong></span>@endif
                                </fieldset>

                                <div class="sizes"><strong> E-mail address:</strong><span
                                            class="star-rating">*</span></div>
                                <fieldset class="{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input type="text" id="email" name="email"
                                           placeholder="Enter the Email Address.....">
                                    @if ($errors->has('email'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('email') }}</strong></span>@endif
                                </fieldset>

                                <div class="sizes"><strong>Department:</strong><span class="star-rating">*</span></div>
                                <fieldset class="{{ $errors->has('department') ? ' has-error' : '' }}">
                                    <input type="text" id="department" name="department"
                                           placeholder="Enter the Department ...">
                                    @if ($errors->has('department'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('department') }}</strong></span>@endif
                                </fieldset>


                                <div class="sizes"><strong>FAX:</strong><span class="star-rating">*</span></div>
                                <fieldset class="{{ $errors->has('fax') ? ' has-error' : '' }}">
                                    <input type="text" id="fax" name="fax" placeholder="Enter the Fax .....">
                                    @if ($errors->has('fax'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('fax') }}</strong></span>@endif
                                </fieldset>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                        <div class="sizes"><strong>Consigner Name:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('consignername') ? ' has-error' : '' }}">
                            <input type="text" id="consignername" name="consignername" placeholder="Enter the Consigner Name....">
                            @if ($errors->has('consignername'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('consignername') }}</strong></span>@endif

                        </fieldset>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">

                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left_right_null">

                                <div class="sizes"><strong> Consigner (To)</strong><span class="star-rating">*</span></div>
                                <fieldset class="{{ $errors->has('mobileno') ? ' has-error' : '' }}">
                                    <input type="text" id="mobileno" name="tomobileno"
                                           placeholder="Enter the  Phone no.....">
                                    @if ($errors->has('mobileno'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('mobileno') }}</strong></span>@endif
                                </fieldset>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left_right_null">

                                <div class="sizes"><strong>Consigner Name:</strong><span class="star-rating">*</span></div>
                                <fieldset class="{{ $errors->has('congname') ? ' has-error' : '' }}">
                                    <input type="text" id="congname" name="congname" placeholder="Consigner Name....">
                                    @if ($errors->has('congname'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('congname') }}</strong></span>@endif

                                </fieldset>
                            </div>

                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null padd_top_10 padd_buttom_20">

                            <div id="flip"> ADD Register</div>
                            <div id="panel">
                                <div class="sizes"><strong>Address:</strong><span class="star-rating">*</span>
                                </div>
                                <fieldset class="{{ $errors->has('congaddress') ? ' has-error' : '' }}">
                                <textarea id="congaddress" name="congaddress" placeholder="Write  address...."
                                          style="height:80px"></textarea>
                                    @if ($errors->has('congaddress'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('congaddress') }}</strong></span>@endif
                                </fieldset>


                                <div class="sizes"><strong>PIN No:</strong><span class="star-rating">*</span></div>
                                <fieldset class="{{ $errors->has('conpin') ? ' has-error' : '' }}">
                                    <input type="text" id="pin" name="conpin" placeholder="Enter the  Pin no.....">
                                    @if ($errors->has('conpin'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('conpin') }}</strong></span>@endif
                                </fieldset>

                                <div class="sizes"><strong>Contact Name:</strong><span class="star-rating">*</span>
                                </div>
                                <fieldset class="{{ $errors->has('congername') ? ' has-error' : '' }}">
                                    <input type="text" id="congername" name="congername"
                                           placeholder="Enter the Contact Name...">
                                    @if ($errors->has('congername'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('congername') }}</strong></span>@endif
                                </fieldset>

                                <div class="sizes"><strong> E-mail address:</strong><span
                                            class="star-rating">*</span></div>
                                <fieldset class="{{ $errors->has('congemail') ? ' has-error' : '' }}">
                                    <input type="text" id="congemail" name="congemail"
                                           placeholder="Enter the Email Address.....">
                                    @if ($errors->has('congemail'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('congemail') }}</strong></span>@endif
                                </fieldset>

                                <div class="sizes"><strong>Department:</strong><span class="star-rating">*</span></div>
                                <fieldset class="{{ $errors->has('congerdepartment') ? ' has-error' : '' }}">
                                    <input type="text" id="congerdepartment" name="congerdepartment"
                                           placeholder="Enter the Department ...">
                                    @if ($errors->has('congerdepartment'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('congerdepartment') }}</strong></span>@endif
                                </fieldset>


                                <div class="sizes"><strong>FAX:</strong><span class="star-rating">*</span></div>
                                <fieldset class="{{ $errors->has('congerfax') ? ' has-error' : '' }}">
                                    <input type="text" id="congerfax" name="congerfax"
                                           placeholder="Enter the Fax .....">
                                    @if ($errors->has('congerfax'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('congerfax') }}</strong></span>@endif
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                    <h4 class="student_subhead">
                        <div class="paid_backcolors consignemt">Consignment Details</div>
                    </h4>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null padd_buttom_10">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">

                        <div class="sizes"><strong>Said to Contain:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('contain') ? ' has-error' : '' }}">
                            <input type="text" id="contain" name="contain"
                                   placeholder="Enter the Said to Contain....">
                            @if ($errors->has('contain'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('contain') }}</strong></span>@endif

                        </fieldset>
                        <div class="sizes col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                            <input type="checkbox" name="x[0]" value="copyattached"/>  Conveyed Copy Attached
                        </div>

                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                        <div class="sizes"><strong>Declared Valued:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('declaredvalued') ? ' has-error' : '' }}">
                            <input type="text" id="declaredvalued" name="declaredvalued"
                                   placeholder="Enter the Consigner Name....">
                            @if ($errors->has('declaredvalued'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('declaredvalued') }}</strong></span>@endif

                        </fieldset>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">

                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left_right_null">

                                <div class="sizes"><strong>Invoice No:</strong><span class="star-rating">*</span>
                                </div>
                                <fieldset class="{{ $errors->has('invoiceno') ? ' has-error' : '' }}">
                                    <input type="text" id="invoiceno" name="invoiceno"
                                           placeholder="Enter the Invoice no.....">
                                    @if ($errors->has('invoiceno'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('invoiceno') }}</strong></span>@endif
                                </fieldset>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left_right_null">

                                <div class="sizes"><strong>Invoice Date: </strong><span class="star-rating">*</span>
                                </div>
                                <fieldset class="{{ $errors->has('dob') ? ' has-error' : '' }}">
                                    <input type="date" id="dob" name="invoicedob">
                                    @if ($errors->has('dob'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('dob') }}</strong></span>@endif

                                </fieldset>
                            </div>
                        </div>

                        <div class="sizes"><strong>Remarks:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('remarks') ? ' has-error' : '' }}">
                            <input type="text" id="remarks" name="remarks" placeholder="Enter the Remarks...">
                            @if ($errors->has('remarks'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('remarks') }}</strong></span>@endif
                        </fieldset>

                    </div>

                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                    <h4 class="student_subhead">
                        <div class="paid_backcolors consignemt">Package Details</div>
                    </h4>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                        <div class="sizes col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                            <input type="radio" name="package" value="No Weight"/> No Weight
                            <input type="radio" name="package" value="Actual Weight"/> Actual Weight
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left_right_null">

                                <div class="sizes"><strong>No of Pkgs:</strong><span class="star-rating">*</span>
                                </div>
                                <fieldset class="{{ $errors->has('pkgs') ? ' has-error' : '' }}">
                                    <input type="text" id="pkgs" name="pkgs" onkeyup="calc()" placeholder="0">
                                    @if ($errors->has('pkgs'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('pkgs') }}</strong></span>@endif
                                </fieldset>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left_right_null">

                                <div class="sizes"><strong>Type of Packing:</strong><span
                                            class="star-rating">*</span></div>
                                <fieldset class="{{ $errors->has('typeofpacking') ? ' has-error' : '' }}">
                                    <select id="typeofpacking" name="typeofpacking">
                                        <option value="">Type of Packing</option>
                                        <option value="Auto Parts">Auto Parts</option>
                                        <option value="BAG">BAG</option>
                                        <option value="Bandal">Bandal</option>
                                        <option value="BARREL">BARREL</option>
                                        <option value="Battery">Battery</option>
                                        <option value="Battery">Battery Case</option>
                                        <option value="BED">BED</option>
                                        <option value="Bill">Bill</option>
                                        <option value="Bitts">Bitts</option>
                                        <option value="Blue Bundle">Blue Bundle</option>
                                        <option value="Box">Box</option>
                                        <option value="Butti">Butti</option>
                                        <option value="Can(Empty)">Can(Empty)</option>
                                        <option value="Cann">Cann</option>
                                        <option value="CARTON">CARTON</option>
                                        <option value="Chair">Chair</option>
                                        <option value="COMPUTER BOX">COMPUTER BOX</option>
                                        <option value="Cover">Cover</option>
                                        <option value="Die">Die</option>
                                        <option value="Drum">Drum</option>
                                        <option value="Engin Bloc">Engin Block</option>
                                        <option value="ENVELOPE">ENVELOPE</option>
                                        <option value="Fan Cartoon">Fan Cartoon</option>
                                        <option value="Food Item">Food Item</option>
                                        <option value="Glass">Glass</option>
                                        <option value="Fragile">Fragile</option>
                                        <option value="Gonny">Gonny</option>
                                        <option value="Gonny Bag Small">Gonny Bag Small</option>
                                        <option value="Green Plastic">Green Plastic</option>
                                        <option value="Hammer">Hammer</option>
                                        <option value="Long Bundle">Long Bundle</option>
                                        <option value="Long Pipe">Long Pipe</option>
                                        <option value="LOOSE">LOOSE</option>
                                        <option value="Machine">Machine</option>
                                        <option value="Mat">Mat</option>
                                        <option value="Metal Sheet">Metal Sheet</option>
                                        <option value="Motor">Motor</option>
                                        <option value="Others">Others</option>
                                        <option value="Packet">Packet</option>
                                        <option value="Parcel">Parcel</option>
                                        <option value="Patti">Patti</option>
                                        <option value="Pipe">Pipe</option>
                                        <option value="Ply">Ply</option>
                                        <option value="Rode Bundle">Rode Bundle</option>
                                        <option value="Role">Role</option>
                                        <option value="Rubber Mat">Rubber Mat</option>
                                        <option value="Saree Parcel">Saree Parcel</option>
                                        <option value="Scooter">Scooter</option>
                                        <option value="Scooty">Scooty</option>
                                        <option value="Scrape">Scrape</option>
                                        <option value="Sheet">Sheet</option>
                                        <option value="STONE">STONE</option>
                                        <option value="Table">Table</option>
                                        <option value="Tanki">Tanki</option>
                                        <option value="Tharmocol">Tharmocol Box</option>
                                        <option value="Thela (Bag)">Thela (Bag)</option>
                                        <option value="Tiles">Tiles</option>
                                        <option value="TIN">TIN</option>
                                        <option value="Tokri">Tokri</option>
                                        <option value="Tube">Tube</option>
                                        <option value="Tube Bandal">Tube Bandal</option>
                                        <option value="TV">TV</option>
                                        <option value="Two Wheeler">Two Wheeler</option>
                                        <option value="Tyre">Tyre</option>
                                        <option value="Tyre Bundle">Tyre Bundle</option>
                                        <option value="Valve">Valve</option>
                                        <option value="White Plastic">White Plastic</option>
                                        <option value="Wire Bundle">Wire Bundle</option>
                                        <option value="Wooden Bandal">Wooden Bandal</option>
                                        <option value="Wooden Box">Wooden Box</option>
                                    </select>
                                    @if ($errors->has('typeofpacking'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('typeofpacking') }}</strong></span>@endif
                                </fieldset>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null padd_top_10">
                            <div class="sizes"><strong> E-Waybill: </strong><span class="star-rating">*</span>
                            </div>
                            <fieldset class="{{ $errors->has('ewaybill') ? ' has-error' : '' }}">
                                <input type="text" id="ewaybill" name="ewaybill"  placeholder="Enter the E-Waybill....">
                                @if ($errors->has('ewaybill'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('ewaybill') }}</strong></span>@endif

                            </fieldset>
                        </div>

                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 "><span
                                    class="sizes"><strong>Weight:</strong><span class="star-rating">*</span></span>
                            <fieldset class="{{ $errors->has('weight') ? ' has-error' : '' }}">
                                <input type="text" id="weight" name="weight" placeholder="0 kg">
                                @if ($errors->has('weight'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('weight') }}</strong></span>@endif
                            </fieldset>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                    <span class="sizes"><strong>Amount:</strong><span
                                                class="star-rating">*</span></span>
                            <fieldset class="{{ $errors->has('amount') ? ' has-error' : '' }}">
                                <input type="text" id="amount" name="amount" onkeyup="calc()" placeholder="0"
                                       style="width: 100%">
                                @if ($errors->has('amount'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('amount') }}</strong></span>@endif
                            </fieldset>
                        </div>

                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null padd_top_20 padd_buttom_20">

                            <div id="flipss"> Charges</div>
                            <div id="panelss">

                                <div class="sizes"><strong>Freight (Rs):</strong><span class="star-rating">*</span>
                                </div>
                                <fieldset class="{{ $errors->has('freight') ? ' has-error' : '' }}">
                                    <input type="text" id="freight" name="freight"  placeholder="0">
                                    @if ($errors->has('freight'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('freight') }}</strong></span>@endif
                                </fieldset>

                                <div class="sizes"><strong>Loading (Rs):</strong><span class="star-rating">*</span>
                                </div>
                                <fieldset class="{{ $errors->has('loading') ? ' has-error' : '' }}">
                                    <input type="text" id="loading" class="txt" name="loading" onkeyup="calculate()" placeholder="0">
                                    @if ($errors->has('loading'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('loading') }}</strong></span>@endif
                                </fieldset>

                                <div class="sizes"><strong>Door Pickup (Rs):</strong><span
                                            class="star-rating">*</span></div>
                                <fieldset class="{{ $errors->has('doorpickup') ? ' has-error' : '' }}">
                                    <input type="text" id="doorpickup" class="txt" name="doorpickup" onkeyup="calc()" placeholder="0">
                                    @if ($errors->has('doorpickup'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('doorpickup') }}</strong></span>@endif
                                </fieldset>

                                <div class="sizes"><strong>Door Delivery (Rs):</strong><span
                                            class="star-rating">*</span></div>
                                <fieldset class="{{ $errors->has('doordelivery') ? ' has-error' : '' }}">
                                    <input type="text" id="doordelivery"  name="doordelivery" onkeyup="calc()" placeholder="0">
                                    @if ($errors->has('doordelivery'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('doordelivery') }}</strong></span>@endif
                                </fieldset>

                                <div class="sizes"><strong>Extra (Rs):</strong><span class="star-rating">*</span>
                                </div>
                                <fieldset class="{{ $errors->has('extra') ? ' has-error' : '' }}">
                                    <input type="text" id="extra"  name="extra" onkeyup="calc()" placeholder="0">
                                    @if ($errors->has('extra'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('extra') }}</strong></span>@endif
                                </fieldset>

                                <div class="sizes"><strong>Total (Rs):</strong><span class="star-rating">*</span>
                                </div>
                                <fieldset class="{{ $errors->has('total') ? ' has-error' : '' }}">
                                    <input type="text" id="total" name="total" placeholder="0">
                                    @if ($errors->has('total'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('total') }}</strong></span>@endif
                                </fieldset>

                                <div class="sizes" ><strong>Grand Total(Rs):</strong><span
                                            class="star-rating">*</span></div>
                                <fieldset class="{{ $errors->has('grandtotal') ? ' has-error' : '' }}">
                                    <input type="text"  id="grandtotal" name="grandtotal"  placeholder="0">
                                    @if ($errors->has('grandtotal'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('grandtotal') }}</strong></span>@endif
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="button_submits col-md-12 col-sm-12 col-xs-12 col-lg-12">
                <input type="submit" value="Submit">&nbsp;
                <input type="reset" value="Clear">
            </div>
        </div>
    </div>
    </form>

    <script>
        function calc() {
            var textValue1 = document.getElementById('amount').value;
            var textValue2 = document.getElementById('pkgs').value;

            document.getElementById('freight').value = textValue1 * textValue2;
            document.getElementById('total').value = textValue1 * textValue2;
            document.getElementById('grandtotal').value = textValue1 * textValue2;
        }
    </script>


    <script>
        $(document).ready(function () {
            $("#flips").click(function () {
                $("#panels").slideToggle("slow");
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $("#flip").click(function () {
                $("#panel").slideToggle("slow");
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $("#flipss").click(function () {
                $("#panelss").slideToggle("slow");
            });
        });
    </script>

    <script>
        $(document).ready(function(){

            //iterate through each textboxes and add keyup
            //handler to trigger sum event
            $(".txt").each(function() {

                $(this).keyup(function(){
                    calculateSum();
                });
            });

        });

        function calculateSum() {

            var sum = 0;
            //iterate through each textboxes and add the values
            $(".txt").each(function() {

                //add only if the value is number
                if(!isNaN(this.value) && this.value.length!=0) {
                    sum += parseFloat(this.value);
                }

            });
            //.toFixed() method will roundoff the final sum to 2 decimal places
            $("#sum").html(sum.toFixed(2));
        }
    </script>



@endsection