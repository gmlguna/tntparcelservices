<html>
<head>
<body>


<form action="/enq" method="post">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <div class="container-fluid">
            <div class="container">
                <div  class="col-md-12 col-sm-12 col-xs-12 col-lg-12 fpr padding_left_right_null padd_top_30 padd_buttom_30">

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 agileits-top staff_form padd_top_30 padd_buttom_30">
                        <form method="post" action="#">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 username  padd_top_30">
                                <fieldset class="{{ $errors->has('enq') ? ' has-error' : '' }}">
                                    <input type="text" class="form-control" id="typeofpacking" placeholder="enq" name="enq">
                                    @if ($errors->has('enq'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('enq') }}</strong></span>@endif
                                    <h5>
                                        <center style="color:red;">
                                            @if(session()->has('message'))
                                                <div class="alert alert-success">
                                                    {{ session()->get('message') }}
                                                </div>
                                            @endif
                                        </center>

                                    </h5>
                                </fieldset>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 button_submitlog">
                                <input type="submit" value="Search">&nbsp;
                            </div>

                        </form>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                    </div>
                </div>
            </div>
        </div>
    </form>


</body>
</head>
</html>

