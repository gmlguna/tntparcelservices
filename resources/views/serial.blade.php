@extends('layout.main')
@section('title', 'Office Center in Krishnagiri')
@section('keywords', 'Office Center in Krishnagiri')
@section('description', 'Office Center in Krishnagiri')
@section('content')

    <h2 class="student_subhead">
        <div class="paid_backcolor"><strong class="welcomekbas"> </strong>
            <ul class="nav navbar-nav welcomekbas">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbas">Operations<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/paidbooking') }}"> Booking<i
                                        class="icon-arrow-right"></i></a>
                        </li>
                        <li>
                            <a href="{{ url('/serial') }}">Dispatch</a>
                        </li>
                        <li>
                            <a href="{{ url('/serial') }}">Receiver</a>
                        </li>
                        <li>
                            <a href="{{ url('/topaybookingdetails') }}">To- Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                        <li>
                            <a href="{{ url('/manualdetails') }}">Manual Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbass">Search<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/user') }}">Serial No</a>
                        </li>
                        <li>
                            <a href="{{ url('/name') }}">Packing Name</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbass">Dispatch<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/serial') }}">Dispatch</a>
                        </li>
                        <li>
                            <a href="{{ url('/dispatch/dispatch_delete') }}">Delivery Details</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/receiver/receiver_delete') }}" class="welcomekbass">Receiver details</a>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/report') }}" class="welcomekbass">Report</a>
                </li>
            </ul>

            <div class="right_logout">
                <a class="logout" href="{{ url('/paidbooking') }}">PAID</a>
                <a class="logout" href="{{ url('/topaybooking') }}">TO PAY</a>
                <a class="logout" href="{{ url('/manualbooking') }}">MANUAL</a>
                <a class="logout" href="login"><strong>Logout</strong></a>
            </div>
        </div>
    </h2>
    <form action="/serial" method="post">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <div class="container-fluid">
            <div class="container">
                <div  class="col-md-12 col-sm-12 col-xs-12 col-lg-12 fpr padding_left_right_null padd_top_30 padd_buttom_30">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_top_30 padd_buttom_30">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 agileits-top staff_form padd_top_30 padd_buttom_30">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 username  padd_top_30">
                                    <select id="destination" class="form-control" name="destination">
                                        <option value="">Destination City</option>
                                        <option value="bhavani">Bhavani</option>
                                        <option value="coimbatore">Coimbatore</option>
                                        <option value="dindigul">Dindigul</option>
                                        <option value="ellampillai">Ellampillai</option>
                                        <option value="erode">Erode</option>
                                        <option value="hosur">Hosur</option>
                                        <option value="jalakandapuram">Jalakandapuram</option>
                                        <option value="mecheri">Mecheri</option>
                                        <option value="omalur">Omalur</option>
                                        <option value="puliyampatti">Puliyampatti</option>
                                        <option value="salem">Salem</option>
                                        <option value="sathyamangalam">Sathyamangalam</option>
                                        <option value="vellore">Vellore</option>
                                    </select>
                                    <h5>
                                    </h5>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 button_submitlog">
                                <input type="submit" value="Search">&nbsp;
                            </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

