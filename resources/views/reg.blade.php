

<html>
<head>
    <title>Sum Html Textbox Values using jQuery/JavaScript</title>
    <style>
        body {
            font-family: sans-serif;
        }
        #summation {
            font-size: 18px;
            font-weight: bold;
            color:#174C68;
        }
        .txt {
            background-color: #FEFFB0;
            font-weight: bold;
            text-align: right;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
</head>
<table width="300px" border="1" style="border-collapse:collapse;background-color:#E8DCFF">
    <tr>
        <td width="40px">1</td>
        <td>Price</td>
        <td><input class="txt" type="text" name="txt"/></td>
    </tr>
    <tr>
        <td>2</td>
        <td>Qty</td>
        <td><input class="txt" type="text" name="txt"/></td>
    </tr>

    <tr id="summation">
        <td>&nbsp;</td>
        <td align="right">Totalamount :</td>
        <td align="center"><span id="sum">0</span></td>
    </tr>
</table>


<script>
    $(document).ready(function(){

        //iterate through each textboxes and add keyup
        //handler to trigger sum event
        $(".txt").each(function() {

            $(this).keyup(function(){
                calculateSum();
            });
        });

    });

    function calculateSum() {

        var sum = 0;
        //iterate through each textboxes and add the values
        $(".txt").each(function() {

            //add only if the value is number
            if(!isNaN(this.value) && this.value.length!=0) {
                sum += parseFloat(this.value);
            }

        });
        //.toFixed() method will roundoff the final sum to 2 decimal places
        $("#sum").html(sum.toFixed(2));
    }
</script>

Price: <input type="text" name="input1" id="input1" onkeyup="calc()" value=""><br>
QTY: <input type="text" name="input2" id="input2" onkeyup="calc()" value="">
<br><a href="javascript: void(0)" onClick="calc()">Total:</a>

<input type="text" name="output" id="output" value="">
<script>
    function calc() {
        var textValue1 = document.getElementById('input1').value;
        var textValue2 = document.getElementById('input2').value;

        document.getElementById('output').value = textValue1 * textValue2;
    }
</script>
</body>
</html>