<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::get('/', function () {
    return view('homes');
});


Route::get('/paid', function () {
    return view('paid');
});

Route::get('/dasboard', function () {
    return view('dasboard');
});

Route::get('/manual', function () {
    return view('manual');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/reg', function () {
    return view('reg');
});

Route::get('/dispatch', function () {
    return view('dispatch');
});

Route::get('/acc', function () {
    return view('acc');
});

Route::get('/serial', function () {
    return view('serial');
});



Route::get('login','UserloginController@index');
Route::post('login','UserloginController@insert');
//Route::post('login','UserloginController@check1');

Route::get('user','ViewController@index');
Route::post('user','ViewController@insert');

Route::get('name','NameController@index');
Route::post('name','NameController@insert');

Route::get('serial','SerialviewController@index');
Route::post('serial','SerialviewController@insert');


Route::get('paid','PaidController@insertform');
Route::post('paid','PaidController@insert');

Route::get('paidbooking','PaidbookingController@insertform');
Route::post('paidbooking','PaidbookingController@insert');

Route::get('paidbooking','PaidbookingdetailsController@index');

Route::get('report','PaidbookingreportController@index');

Route::get('report','DispatchreportController@index');

Route::get('paidbookingdetails','PaidbookingdetailsController@index');

//Route::get('serial','SerialviewController@index');

//Route::get('','CaldetailsController@index');

Route::get('paid/paid_delete','Paid_deleteController@index');
Route::get('paid/delete/{id}','Paid_deleteController@destroy');

Route::get('paid/paid_edit','Paid_updateController@index');
Route::get('paid/edit/{id}','Paid_updateController@show');
Route::post('paid/edit/{id}','Paid_updateController@edit');


Route::get('topaybooking','TopayController@insertform');
Route::post('topaybooking','TopayController@insert');

Route::get('topaybookingdetails','TopaybookingdetailsController@index');

Route::get('topay/topay_delete','Topay_deleteController@index');
Route::get('topay/delete/{id}','Topay_deleteController@destroy');

Route::get('topay/topay_edit','Topay_updateController@index');
Route::get('topay/edit/{id}','Topay_updateController@show');
Route::post('topay/edit/{id}','Topay_updateController@edit');


Route::get('manualbooking','ManualController@insertform');
Route::post('manualbooking','ManualController@insert');

Route::get('manualdetails','ManualdetailsController@index');

Route::get('manual/manual_delete','Manual_deleteController@index');
Route::get('manual/delete/{id}','Manual_deleteController@destroy');

Route::get('manual/manual_edit','Manual_updateController@index');
Route::get('manual/edit/{id}','Manual_updateController@show');
Route::post('manual/edit/{id}','Manual_updateController@edit');

Route::get('check/{user_id}','PdfController@index');

Route::get('dispatch','DispatchController@insertform');
Route::post('dispatch','DispatchController@insert');

Route::get('dispatchdetails','DispatchdetailsController@index');

Route::get('dispatch','DispatchviewController@index');

Route::get('dispatch/dispatch_delete','Dispatch_deleteController@index');
Route::get('dispatch/delete/{id}','Dispatch_deleteController@destroy');

Route::get('dispatch/dispatch_edit','Dispatch_updateController@index');
Route::get('dispatch/edit/{id}','Dispatch_updateController@show');
Route::post('dispatch/edit/{id}','Dispatch_updateController@edit');

Route::get('serial','ReceiverController@index');
Route::get('edit/{id}','ReceiverController@show');
Route::post('edit/{id}','ReceiverController@edit');

Route::get('receiver','ReceiveController@insertform');
Route::post('receiver','ReceiveController@insert');

Route::get('receiver/receiver_delete','Receiver_deleteController@index');
Route::get('receiver/delete/{id}','Receiver_deleteController@destroy');


Route::get('serial','DeliveryController@index');
Route::get('edits/{id}','DeliveryController@show');
Route::post('edits/{id}','DeliveryController@edits');



Route::get('one','OneController@insertform');
Route::post('one','OneController@insert');

Route::get('one','OneViewController@index');

Route::get('enq','EnqController@index');
Route::post('enq','EnqController@insert');

Route::get('order','OrderController@insertform');
Route::post('order','OrderController@insert');

Route::get('order','OrderViewController@index');