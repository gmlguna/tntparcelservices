<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class EnqController extends Controller
{
    public function index()
    {
        return view('enq');
    }

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'enq' => 'enq|unique :ones',


        ]);
    }


    public function insert(Request $request)
    {
        $request->validate([
            'enq' => 'required',


        ]);

        if ($login = DB::table('ones')->where('enq', $request->enq)->first()) {

            $enq = $request->enq;
            return view('enqview', compact('enq','login'));

        } else {

            return redirect()->back()->with('message', ' enq incorrect.');
//            return view('hi');
        }




    }

//    public function check1(Request $request)
//    {
//        $users = DB::table('reg')->where('username', $request->username)->where('password', $request->password)->get();
//        return view('guna/first')->with('users', $users);
//
//    }

}
