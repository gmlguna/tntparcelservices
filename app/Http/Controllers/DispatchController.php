<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Validator;
use Validator;

class DispatchController extends Controller
{
    public function insertform()
    {
        return view('dispatch');
    }

    /**
     * @param Request $request
     */

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'dispatchno' => 'required',
            'vehicleno' => 'required',
            'drivername' => 'required',
            'cleanername' => 'required',
            'dispatchdate' => 'required',
            'destination' => 'required',

        ]);
    }


    public function insert(Request $request)
    {

        $request->validate([
            'dispatchno' => 'required',
            'vehicleno' => 'required',
            'drivername' => 'required',
            'cleanername' => 'required',
            'dispatchdate' => 'required',
            'destination' => 'required',
        ]);


        $dispatchno = $request->dispatchno;
        $invoicedob = $request->invoicedob;
        $consignername = $request->consignername;
        $congname = $request->congname;
        $destinationcity = $request->destinationcity;
        $branch = $request->branch;
        $typeofpacking = $request->typeofpacking;
        $pkgs = $request->pkgs;
        $weight = $request->weight;
        $amount = $request->amount;
        $freight = $request->freight;
        $grandtotal = $request->grandtotal;
        $vehicleno= $request->vehicleno;
        $drivername = $request->drivername;
        $cleanername = $request->cleanername;
        $dispatchdate = $request->dispatchdate;
        $destination = $request->destination;

//        var_dump($request->all());die;
        DB::insert('insert into dispatch (dispatchno,  invoicedob, consignername, congname, destinationcity, branch, typeofpacking, pkgs, weight, amount, freight, grandtotal, vehicleno, drivername, cleanername, dispatchdate, destination) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [$dispatchno, $invoicedob, $consignername, $congname, $destinationcity, $branch, $typeofpacking, $pkgs, $weight, $amount, $freight, $grandtotal, $vehicleno, $drivername, $cleanername, $dispatchdate, $destination]);

//          return view('traningclass');
//        echo "Record inserted successfully.";
        return redirect()->back()->with ('message','Delivery Booking success ');

//
    }
}