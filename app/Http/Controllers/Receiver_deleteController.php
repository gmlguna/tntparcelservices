<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Receiver_deleteController extends Controller {
    public function index(){
        $users = DB::select('select * from receiver');
        return view('receiver/receiver_delete',['users'=>$users]);
    }
    public function destroy($id) {
        DB::delete('delete from receiver where  id= ?',[$id]);
        return redirect()->back()->with ('message',' Receiver Details Deleted ');
    }
}