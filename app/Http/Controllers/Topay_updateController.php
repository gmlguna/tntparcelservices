<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Topay_updateController extends Controller {
    public function index(){
        $users = DB::select('select * from topay');
        return view('topay/topay_edit',['users'=>$users]);
    }
    public function show($id) {
        $users = DB::select('select * from topay where id = ?',[$id]);
        return view('topay/topay_update',['users'=>$users]);
    }
    public function edit(Request $request,$id) {
        $id = $request->id;
        $pkgs = $request->pkgs;
        $destination = $request->destination;
        $typeofpacking = $request->typeofpacking;
        $amount = $request->amount;
        $congname = $request->congname;
        $contactname = $request->contactname;
        $invoiceno = $request->invoiceno;
        $branch = $request->branch;
        $remarks = $request->remarks;
        $invoicedob = $request->invoicedob;
        $grandtotal = $request->grandtotal;

        DB::table('topay')
            ->where('id', $id)
            ->update(['id' => $id,'pkgs' => $pkgs,'destination' => $destination,'typeofpacking' => $typeofpacking,'amount' => $amount,'congname' => $congname,'contactname' => $contactname,'invoiceno' => $invoiceno,'branch' => $branch, 'remarks' => $remarks, 'invoicedob' => $invoicedob, 'grandtotal' => $grandtotal]);

//        DB::update('update staff set name = ?, mobileno = ?, where staff_id = ?',[$name, $mobileno, $staff_id]);
        return redirect()->back()->with ('message',' Topay Booking details upadeted ');
    }
}