<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Paid_deleteController extends Controller {
    public function index(){
        $users = DB::select('select * from paid');
        return view('paid/paid_delete',['users'=>$users]);
    }
    public function destroy($id) {
        DB::delete('delete from paid where  id= ?',[$id]);
        return redirect()->back()->with ('message',' Paid Booking Details Deleted ');
    }
}