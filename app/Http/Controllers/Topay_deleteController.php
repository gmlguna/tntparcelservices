<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Topay_deleteController extends Controller {
    public function index(){
        $users = DB::select('select * from topay');
        return view('topay/topay_delete',['users'=>$users]);
    }
    public function destroy($id) {
        DB::delete('delete from topay where  id= ?',[$id]);
        return redirect()->back()->with ('message',' Topay Booking Details Deleted ');
    }
}