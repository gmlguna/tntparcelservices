<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class NameController extends Controller
{
    public function index()
    {
        return view('name');
    }

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'typeofpacking' => 'required|unique :paid',


        ]);
    }


    public function insert(Request $request)
    {
        $request->validate([
            'typeofpacking' => 'required',


        ]);

        if ($login = DB::table('paid')->where('typeofpacking', $request->typeofpacking)->first()) {

            $typeofpacking = $request->typeofpacking;
            return view('nameview', compact('typeofpacking','login'));

        } else {

            return redirect()->back()->with('message', ' The password that you\'ve entered is incorrect.');
//            return view('hi');
        }




    }

//    public function check1(Request $request)
//    {
//        $users = DB::table('reg')->where('username', $request->username)->where('password', $request->password)->get();
//        return view('guna/first')->with('users', $users);
//
//    }

}
