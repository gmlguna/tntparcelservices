<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserloginController extends Controller
{
    public function index()
    {
        return view('login');
    }

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|unique :userlogin',
            'pwd' => 'required',

        ]);
    }

    public function insert(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'pwd' => 'required',

        ]);

        if ($login = DB::table('userlogin')->where('username', $request->username)->where('pwd', $request->pwd)->first()) {

            $username = $request->username;
            return view('dasboard', compact('username','login'));

        } else {

            return redirect()->back()->with('message', ' The password that you\'ve entered is incorrect.');
//            return view('hi');
        }



    }

//    public function check1(Request $request)
//    {
//        $users = DB::table('userlogin')->where('username', $request->username)->where('pwd', $request->pwd)->get();
//        return view('paidbooking')->with('users', $users);
//
//    }

}
