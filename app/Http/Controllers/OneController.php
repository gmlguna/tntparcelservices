<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Validator;
use Validator;

class OneController extends Controller
{
    public function insertform()
    {
        return view('one');
    }

    /**
     * @param Request $request
     */

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'email' => 'required',
            'address' => 'required',
            'date' => 'required',
            'enq' => 'required',


        ]);
    }


    public function insert(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'address' => 'required',
            'date' => 'required',
            'enq' => 'required',
        ]);


        $name = $request->name;
        $email = $request->email;
        $address = $request->address;
        $date = $request->date;
        $enq = $request->enq;


//        var_dump($request->all());die;
        DB::insert('insert into ones (name,  email, address, date, enq) values(?,?,?,?,?)', [$name,  $email, $address, $date, $enq]);

//          return view('traningclass');
//        echo "Record inserted successfully.";
        return redirect()->back()->with ('message','success ');

//
    }
}