<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReceiveController extends Controller
{
    public function insertform()
    {
        return view('receiver');
    }

    public function insert(Request $request)
    {
        $serial = $request->serial;
        $invoicedob = $request->invoicedob;
        $consignername = $request->consignername;
        $congname = $request->congname;
        $destination = $request->destination;
        $branch = $request->branch;
        $typeofpacking = $request->typeofpacking;
        $pkgs = $request->pkgs;
        $weight = $request->weight;
        $amount = $request->amount;
        $freight = $request->freight;
        $grandtotal = $request->grandtotal;


//        var_dump($request->all());die;
        DB::insert('insert into receiver (serial, invoicedob, consignername, congname, destination, branch, typeofpacking, pkgs, weight, amount, freight, grandtotal) values(?,?,?,?,?,?,?,?,?,?,?,?)', [$serial, $invoicedob, $consignername, $congname, $destination, $branch, $typeofpacking, $pkgs, $weight, $amount, $freight, $grandtotal]);

//          return view('traningclass');
//        echo "Record inserted successfully.";
        return redirect()->back()->with ('message','Booking Received ');


    }



}