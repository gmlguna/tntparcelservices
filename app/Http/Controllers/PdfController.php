<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\product;
use DB;
use App\Http\Requests;

class PdfController extends Controller
{
    public function index($id)
    {
        $info = DB::table('paid')->where('id', $id)->first();
        $data = ['id' =>$info->id ,
            'invoicedob'=>$info->invoicedob,
            'mobileno'=>$info->mobileno,
            'consignername'=>$info->consignername,
            'tomobileno'=>$info->tomobileno,
            'congname'=>$info->congname,
            'destination'=>$info->destination,
            'branch'=>$info->branch,
            'weight'=>$info->weight,
            'pkgs'=>$info->pkgs,
            'typeofpacking'=>$info->typeofpacking,
            'amount'=>$info->amount,
            'freight'=>$info->freight,
            'total'=>$info->total,
            'grandtotal'=>$info->grandtotal,];
        $pdf = \PDF::loadView('pdf',compact('data'));
        return $pdf->download();

    }
}
