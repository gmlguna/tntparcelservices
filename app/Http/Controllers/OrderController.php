<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Validator;
use Validator;

class OrderController extends Controller
{
    public function insertform()
    {
        return view('order');
    }

    /**
     * @param Request $request
     */

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'date' => 'required',
            'price' => 'required',
            'time' => 'time|unique :orders',


        ]);
    }

    public function insert(Request $request)
    {

        $username_exist = DB::table('orders')->where('time','=',$request->time)->first();

        if($username_exist){
            return redirect()->back()->with('msg', 'sorry your time is complete, tomorrow booking ');
        }

        $request->validate([
            'name' => 'required',
            'date' => 'required',
            'price' => 'required',
            'time' => 'required',
        ]);

        $name = $request->name;
        $date = $request->date;
        $price = $request->price;
        $time = $request->time;


//        var_dump($request->all());die;
        DB::insert('insert into orders (name, date, price, time) values(?,?,?,?)', [$name, $date, $price, $time]);

//          return view('traningclass');
//        echo "Record inserted successfully.";
        return redirect()->back()->with ('message','Order ');

//
    }
}