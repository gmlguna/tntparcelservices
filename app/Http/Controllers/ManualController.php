<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Validator;
use Validator;

class ManualController extends Controller
{
    public function insertform()
    {
        return view('manualbooking');
    }

    /**
     * @param Request $request
     */

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'paid' => 'required',
            'nodocument' => 'required',
            'sourcecity' => 'required',
            'sourcebranch' => 'required',
            'destinationcity' => 'required',
            'selectbranch' => 'required',
            'waybillno' => 'required',
            'waybilldate' => 'required',
            'mobileno' => 'required',
            'consignername' => 'required',
            'tomobileno' => 'required',
            'congname' => 'required',
            'address' => 'required',
            'pin' => 'required',
            'contactname' => 'required',
            'email' => 'required',
            'department' => 'required',
            'fax' => 'required',
            'congaddress' => 'required',
            'conpin' => 'required',
            'congername' => 'required',
            'congemail' => 'required',
            'congerdepartment' => 'required',
            'congerfax' => 'required',
            'contain' => 'required',
            'declaredvalued' => 'required',
            'invoiceno' => 'required',
            'invoicedob' => 'required',
            'remarks' => 'required',
            'package' => 'required',
            'pkgs' => 'required',
            'typeofpacking' => 'required',
            'ewaybill' => 'required',
            'weight' => 'required',
            'amount' => 'required',
            'freight' => 'required',
            'loading' => 'required',
            'doorpickup' => 'required',
            'doordelivery' => 'required',
            'extra' => 'required',
            'total' => 'required',
            'grandtotal' => 'required',

        ]);
    }


    public function insert(Request $request)
    {

        $request->validate([
            'paid' => 'required',
            'nodocument' => 'required',
            'sourcecity' => 'required',
            'sourcebranch' => 'required',
            'destinationcity' => 'required',
            'selectbranch' => 'required',
            'waybillno' => 'required',
            'waybilldate' => 'required',
            'mobileno' => 'required',
            'consignername' => 'required',
            'tomobileno' => 'required',
            'congname' => 'required',
            'address' => 'required',
            'pin' => 'required',
            'contactname' => 'required',
            'email' => 'required',
            'department' => 'required',
            'fax' => 'required',
            'congaddress' => 'required',
            'conpin' => 'required',
            'congername' => 'required',
            'congemail' => 'required',
            'congerdepartment' => 'required',
            'congerfax' => 'required',
            'contain' => 'required',
            'declaredvalued' => 'required',
            'invoiceno' => 'required',
            'invoicedob' => 'required',
            'remarks' => 'required',
            'package' => 'required',
            'pkgs' => 'required',
            'typeofpacking' => 'required',
            'ewaybill' => 'required',
            'weight' => 'required',
            'amount' => 'required',
            'freight' => 'required',
            'loading' => 'required',
            'doorpickup' => 'required',
            'doordelivery' => 'required',
            'extra' => 'required',
            'total' => 'required',
            'grandtotal' => 'required',

        ]);


        $paid = $request->paid;
        $nodocument = $request->nodocument;
        $sourcecity = $request->sourcecity;
        $sourcebranch= $request->sourcebranch;
        $destinationcity = $request->destinationcity;
        $selectbranch = $request->selectbranch;
        $waybillno = $request->waybillno;
        $waybilldate = $request->waybilldate;
        $mobileno = $request->mobileno;
        $consignername = $request->consignername;
        $tomobileno = $request->tomobileno;
        $congname = $request->congname;
        $address = $request->address;
        $pin = $request->pin;
        $contactname = $request->contactname;
        $email = $request->email;
        $department = $request->department;
        $fax = $request->fax;
        $congaddress = $request->congaddress;
        $conpin = $request->conpin;
        $congername = $request->congername;
        $congemail = $request->congemail;
        $congerdepartment = $request->congerdepartment;
        $congerfax = $request->congerfax;
        $contain = $request->contain;
        $declaredvalued = $request->declaredvalued;
        $invoiceno = $request->invoiceno;
        $invoicedob = $request->invoicedob;
        $remarks = $request->remarks;
        $package = $request->package;
        $pkgs = $request->pkgs;
        $typeofpacking = $request->typeofpacking;
        $ewaybill = $request->ewaybill;
        $weight = $request->weight;
        $amount = $request->amount;
        $freight = $request->freight;
        $loading = $request->loading;
        $doorpickup= $request->doorpickup;
        $doordelivery = $request->doordelivery;
        $extra = $request->extra;
        $total = $request->total;
        $grandtotal = $request->grandtotal;



//        var_dump($request->all());die;
        DB::insert('insert into manual (paid, nodocument, sourcecity, sourcebranch, destinationcity, selectbranch, waybillno, waybilldate, mobileno, consignername, tomobileno, congname,  address, pin, contactname, email, department, fax, congaddress, conpin, congername, congemail, congerdepartment, congerfax, contain, declaredvalued, invoiceno, invoicedob, remarks, package, pkgs, typeofpacking, ewaybill, weight, amount, freight, loading, doorpickup, doordelivery, extra, total, grandtotal) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [$paid, $nodocument, $sourcecity, $sourcebranch, $destinationcity, $selectbranch, $waybillno, $waybilldate, $mobileno, $consignername, $tomobileno, $congname, $address, $pin, $contactname, $email, $department, $fax, $congaddress, $conpin, $congername, $congemail, $congerdepartment, $congerfax, $contain, $declaredvalued, $invoiceno, $invoicedob, $remarks, $package, $pkgs, $typeofpacking, $ewaybill, $weight, $amount, $freight, $loading, $doorpickup, $doordelivery, $extra, $total, $grandtotal]);

//          return view('traningclass');
//        echo "Record inserted successfully.";
        return redirect()->back()->with ('message','Manual Booking success ');

//
    }
}