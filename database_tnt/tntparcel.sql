-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 23, 2018 at 03:52 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tntparcel`
--

-- --------------------------------------------------------

--
-- Table structure for table `dispatch`
--

CREATE TABLE `dispatch` (
  `id` int(11) NOT NULL,
  `dispatchno` varchar(100) NOT NULL,
  `invoicedob` varchar(60) NOT NULL,
  `consignername` varchar(100) NOT NULL,
  `congname` varchar(100) NOT NULL,
  `destinationcity` varchar(100) NOT NULL,
  `branch` varchar(100) NOT NULL,
  `typeofpacking` varchar(100) NOT NULL,
  `pkgs` varchar(100) NOT NULL,
  `weight` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `freight` varchar(100) NOT NULL,
  `grandtotal` varchar(100) NOT NULL,
  `vehicleno` varchar(100) NOT NULL,
  `drivername` varchar(100) NOT NULL,
  `cleanername` varchar(100) NOT NULL,
  `dispatchdate` varchar(100) NOT NULL,
  `destination` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dispatch`
--

INSERT INTO `dispatch` (`id`, `dispatchno`, `invoicedob`, `consignername`, `congname`, `destinationcity`, `branch`, `typeofpacking`, `pkgs`, `weight`, `amount`, `freight`, `grandtotal`, `vehicleno`, `drivername`, `cleanername`, `dispatchdate`, `destination`) VALUES
(18, '100', '2018-09-28', 'dfdfdfd', 'fsdfsdf', 'coimbatore', 'coimbatore', 'Blue Bundle', '0', '50', '10', '500', '520', '4545', 'rwetre', 'gfgfdgf', '28/09/18', 'Bangalore');

-- --------------------------------------------------------

--
-- Table structure for table `manual`
--

CREATE TABLE `manual` (
  `id` int(11) NOT NULL,
  `paid` varchar(100) NOT NULL,
  `nodocument` varchar(100) NOT NULL,
  `sourcecity` varchar(100) NOT NULL,
  `sourcebranch` varchar(100) NOT NULL,
  `destinationcity` varchar(100) NOT NULL,
  `selectbranch` varchar(100) NOT NULL,
  `waybillno` varchar(100) NOT NULL,
  `waybilldate` varchar(100) NOT NULL,
  `mobileno` varchar(100) NOT NULL,
  `consignername` varchar(100) NOT NULL,
  `tomobileno` varchar(100) NOT NULL,
  `congname` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `pin` varchar(100) NOT NULL,
  `contactname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `congaddress` varchar(100) NOT NULL,
  `conpin` varchar(100) NOT NULL,
  `congername` varchar(100) NOT NULL,
  `congemail` varchar(100) NOT NULL,
  `congerdepartment` varchar(100) NOT NULL,
  `congerfax` varchar(100) NOT NULL,
  `contain` varchar(100) NOT NULL,
  `declaredvalued` varchar(100) NOT NULL,
  `invoiceno` varchar(100) NOT NULL,
  `invoicedob` varchar(100) NOT NULL,
  `remarks` varchar(100) NOT NULL,
  `package` varchar(100) NOT NULL,
  `pkgs` varchar(100) NOT NULL,
  `typeofpacking` varchar(100) NOT NULL,
  `ewaybill` varchar(100) NOT NULL,
  `weight` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `freight` double NOT NULL,
  `loading` double NOT NULL,
  `doorpickup` double NOT NULL,
  `doordelivery` double NOT NULL,
  `extra` double NOT NULL,
  `total` double NOT NULL,
  `grandtotal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manual`
--

INSERT INTO `manual` (`id`, `paid`, `nodocument`, `sourcecity`, `sourcebranch`, `destinationcity`, `selectbranch`, `waybillno`, `waybilldate`, `mobileno`, `consignername`, `tomobileno`, `congname`, `address`, `pin`, `contactname`, `email`, `department`, `fax`, `congaddress`, `conpin`, `congername`, `congemail`, `congerdepartment`, `congerfax`, `contain`, `declaredvalued`, `invoiceno`, `invoicedob`, `remarks`, `package`, `pkgs`, `typeofpacking`, `ewaybill`, `weight`, `amount`, `freight`, `loading`, `doorpickup`, `doordelivery`, `extra`, `total`, `grandtotal`) VALUES
(6, 'Paid', 'document', '624', 'sourcebranch', '628', 'sourcebranch', '80', '2018-08-13', '9087654321', 'thala', '986554232', 'fgfdgd', 'kurumberi', '12455', 'dfsdfgd', 'guna@gmail.com', 'gfds', 'good', 'vellore', '12455', 'gunasekaran', 'guna@gmail.com', 'mca', 'good', 'thiru', 'dfdfdf', '1234', '2018-08-07', 'good', 'Actual Weight', '27', '307', '456', '40kg', '20', 49, 0, 0, 0, 0, 49, 49),
(8, 'Paid', 'document', '307', 'sourcebranch', '307', 'sourcebranch', '70', '2018-08-14', 'fgsfdgfg', 'rtsdgsdfg', '56366', 'fgfdgd', 'gunasekaran', '345345', 'dfsdfgd', 'guna1081994@gmail.com', 'mca', 'good', 'dfds', '12455', 'gunasekaran', 'sekar@gmail.com', 'mca', 'bad', 'good', 'laptop', '1234', '2018-07-31', 'good', 'Actual Weight', '56', '307', '0', '40kg', '2', 112, 0, 0, 0, 0, 112, 112),
(9, 'Paid', 'document', '307', 'sourcebranch', '307', 'sourcebranch', '56', '2018-08-23', '9087654321', 'thala', '56366', 'fgfdgd', 'sadf', '345345', 'kurumberi', 'guna1081994@gmail.com', 'mca', 'good', 'dfda', '56345', 'gunasekaran', 'guna@gmail.com', 'dffad', 'good', 'good', 'dfdfdf', '12345', '2018-08-07', 'good', 'Actual Weight', '5', '432', '0', '40kg', '20', 100, 0, 0, 0, 0, 100, 100);

-- --------------------------------------------------------

--
-- Table structure for table `ones`
--

CREATE TABLE `ones` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `enq` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ones`
--

INSERT INTO `ones` (`id`, `name`, `email`, `address`, `date`, `enq`) VALUES
(4, 'madhi', 'me@gmail.com', 'gunasekaran', '2018-09-14', 'one'),
(5, 'deapan', 'me@gmail.com', 'chennai', '2018-09-14', 'two'),
(6, 'karthi', 'gunaseakran@gmail.com', 'kurumberi', '2018-09-14', 'three'),
(8, 'vijya', 'vijya@gmail.com', 'vellore', '2018-09-15', 'four'),
(9, 'bluebox_bootstrap', 'guna1081994@gmail.com', 'chennai', '2018-09-05', 'six');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `price` varchar(100) NOT NULL,
  `time` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `name`, `date`, `price`, `time`) VALUES
(1, 'book', '2018-09-14', '50', ''),
(2, 'muse', '2018-09-14', '40', '9:00AM – 11:00AMh'),
(3, 'guna', '2018-09-14', '40', '11:59:44'),
(4, 'madhi', '2018-09-14', '50', '9:00AM – 11:00AMs'),
(5, 'guna', '2018-09-14', '50', '9:00AM – 11:00AMd'),
(6, 'madhan', '2018-09-05', '40', '9:00AM – 11:00AoM'),
(7, 'madhi', '2018-08-27', '50', '11s:15AM – 1:00PM');

-- --------------------------------------------------------

--
-- Table structure for table `padidetails`
--

CREATE TABLE `padidetails` (
  `id` int(11) NOT NULL,
  `document` varchar(100) NOT NULL,
  `mobileno` varchar(100) NOT NULL,
  `destination` varchar(100) NOT NULL,
  `consignername` varchar(100) NOT NULL,
  `branch` varchar(100) NOT NULL,
  `tomobileno` varchar(100) NOT NULL,
  `congname` varchar(100) NOT NULL,
  `package` varchar(100) NOT NULL,
  `pkgs` varchar(100) NOT NULL,
  `typeofpacking` varchar(100) NOT NULL,
  `ewaybill` varchar(100) NOT NULL,
  `weight` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `freight` int(100) NOT NULL,
  `total` varchar(100) NOT NULL,
  `grandtotal` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `padidetails`
--

INSERT INTO `padidetails` (`id`, `document`, `mobileno`, `destination`, `consignername`, `branch`, `tomobileno`, `congname`, `package`, `pkgs`, `typeofpacking`, `ewaybill`, `weight`, `amount`, `freight`, `total`, `grandtotal`) VALUES
(1, 'document', '1234456676', 'bhavani', 'thala', 'bhavani', '5634465', 'thala', 'Actual Weight', '4', 'CARTON', '55', '10kg', '67', 268, '268', '268'),
(2, 'document', '9876543210', 'bhavani', 'thala', 'coimbatore', '5634465', 'thalas', 'Actual Weight', '56', 'Chair', 'dfsdg', '10kg', '98', 5488, '5488', '5488'),
(3, 'document', '1234456676', 'bhavani', 'thala', 'bhavani', '56366', 'thala', 'Actual Weight', '5', 'BAG', '456', '10kg', '30', 150, '150', '150'),
(4, 'nodocument', '9876543210', 'coimbatore', 'gdfs', 'coimbatore', '5634465', 'thala', 'Actual Weight', '56', 'Box', '456', '10kg', '78', 4368, '4368', '4368'),
(5, 'nodocument', '1234456676', 'coimbatore', 'gdfs', 'coimbatore', '56366', 'fgfdgd', 'No Weight', '5', 'Can(Empty)', '60', '66', '6', 30, '30', '30'),
(6, 'document', '1234456676', 'coimbatore', 'thala', 'coimbatore', '5634465', 'thala', 'Actual Weight', '6', 'Auto Parts', '456', '40kg', '6', 36, '36', '36'),
(7, 'document', '1234456676', 'bhavani', 'thala', 'coimbatore', '56366', 'fgfdgd', 'Actual Weight', '5', 'BAG', '456', '66', '6', 30, '30', '30'),
(8, 'document', '12345678798', 'coimbatore', 'thala', 'ellampillai', 'chnnai', 'fgfdgd', 'Actual Weight', '4', 'Box', '55', '77', '4', 16, '16', '16'),
(9, 'nodocument', '9876543210', 'coimbatore', 'thala', 'bhavani', '56366', 'thala', 'Actual Weight', '5', 'Battery', '0', '10kg', '7', 35, '35', '35'),
(10, 'document', '9087654321', 'dindigul', 'gdfs', 'ellampillai', '5634465', 'thala', 'Actual Weight', '5', 'CARTON', 'dfsdg', '55', '55', 275, '275', '275'),
(11, 'nodocument', '9876543210', 'coimbatore', 'rtsdgsdfg', 'bhavani', '5634465', 'fgfsdg', 'Actual Weight', '6', 'CARTON', 'dfsdg', '40kg', '6', 36, '36', '36'),
(12, 'document', '1234456676', 'dindigul', 'gdfs', 'dindigul', '5634465', 'thala', 'Actual Weight', '5', 'Chair', '5', '40kg', '55', 275, '275', '275'),
(13, 'nodocument', '55', 'bhavani', 'gdfs', 'dindigul', '5634465', 'thala', 'Actual Weight', '5', 'Chair', 'dfsdg', '55', '100', 500, '500', '500'),
(14, 'document', '9876543210', 'coimbatore', 'thala', 'coimbatore', '5634465', 'thala', 'Actual Weight', '6', 'Chair', '6', '66', '66', 396, '396', '396');

-- --------------------------------------------------------

--
-- Table structure for table `paid`
--

CREATE TABLE `paid` (
  `id` int(11) NOT NULL,
  `document` varchar(100) NOT NULL,
  `mobileno` varchar(100) NOT NULL,
  `destination` varchar(100) NOT NULL,
  `consignername` varchar(100) NOT NULL,
  `branch` varchar(100) NOT NULL,
  `tomobileno` varchar(100) NOT NULL,
  `congname` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `pin` varchar(60) NOT NULL,
  `contactname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `congaddress` varchar(100) NOT NULL,
  `conpin` varchar(100) NOT NULL,
  `congername` varchar(100) NOT NULL,
  `congemail` varchar(100) NOT NULL,
  `congerdepartment` varchar(100) NOT NULL,
  `congerfax` varchar(100) NOT NULL,
  `contain` varchar(100) NOT NULL,
  `declaredvalued` varchar(100) NOT NULL,
  `invoiceno` varchar(100) NOT NULL,
  `invoicedob` varchar(100) NOT NULL,
  `remarks` varchar(100) NOT NULL,
  `package` varchar(100) NOT NULL,
  `pkgs` varchar(100) NOT NULL,
  `typeofpacking` varchar(100) NOT NULL,
  `ewaybill` varchar(100) NOT NULL,
  `weight` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `freight` double NOT NULL,
  `loading` double NOT NULL,
  `doorpickup` double NOT NULL,
  `doordelivery` double NOT NULL,
  `extra` double NOT NULL,
  `total` double NOT NULL,
  `grandtotal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paid`
--

INSERT INTO `paid` (`id`, `document`, `mobileno`, `destination`, `consignername`, `branch`, `tomobileno`, `congname`, `address`, `pin`, `contactname`, `email`, `department`, `fax`, `congaddress`, `conpin`, `congername`, `congemail`, `congerdepartment`, `congerfax`, `contain`, `declaredvalued`, `invoiceno`, `invoicedob`, `remarks`, `package`, `pkgs`, `typeofpacking`, `ewaybill`, `weight`, `amount`, `freight`, `loading`, `doorpickup`, `doordelivery`, `extra`, `total`, `grandtotal`) VALUES
(94, 'document', '1234456676', 'ellampillai', 'guna', 'bhavani', '5634465', 'thala', 'address', 'pin', 'contactname', 'email', 'department', 'fax', 'congaddress', 'conpin', 'congername', 'congemail', 'congerdepartment', 'congerfax', 'contain', 'declaredvalued', 'invoiceno', '2018-09-07', 'remarks', 'Actual Weight', '0', 'Blue Bundle', '6767', '60', '6', 360, 1, 2, 4, 5, 360, 372),
(95, 'nodocument', '1234456676', 'jalakandapuram', 'guna', 'coimbatore', '56366', '56366', 'kurumberi', '345345', 'gunasekaran', 'gunasekaran1081994@123', 'mca', '3443', 'titupatturi', '3343', 'madhan', 'guna@gmail.com', 'm', '534', 'centercontain', 'caluculation', '454', '2018-09-07', 'goods', 'Actual Weight', '0', 'Engin Bloc', '9090', '50kg', '6', 300, 0, 0, 0, 0, 300, 300),
(96, 'document', '1234456676', 'ellampillai', 'thala', 'dindigul', '5634465', '5634465', 'address', 'pin', 'contactname', 'email', 'department', 'fax', 'congaddress', 'conpin', 'congername', 'congemail', 'congerdepartment', 'congerfax', 'contain', 'declaredvalued', 'invoiceno', '2018-09-27', 'remarks', 'No Weight', '55', 'Chair', '0', '0', '5', 275, 0, 0, 0, 0, 275, 275),
(97, 'document', '1234456676', 'coimbatore', 'thala', 'select', '56366', 'sankar', 'address', 'pin', 'contactname', 'email', 'department', 'fax', 'congaddress', 'conpin', 'congername', 'congemail', 'congerdepartment', 'congerfax', 'contain', 'declaredvalued', 'invoiceno', '2018-09-13', 'remarks', 'No Weight', '56', 'Bandal', '0', '0', '50', 2800, 0, 0, 0, 0, 2800, 2800),
(98, 'document', '1234456676', 'dindigul', 'guna', 'dindigul', '5634465', '5634465', 'address', 'pin', 'contactname', 'email', 'department', 'fax', 'congaddress', 'conpin', 'congername', 'congemail', 'congerdepartment', 'congerfax', 'contain', 'declaredvalued', 'invoiceno', '2018-09-13', 'remarks', 'Actual Weight', '0', 'Bandal', '0', '500', '5', 2500, 0, 0, 0, 0, 2500, 2500),
(99, 'document', '6760563405605', 'dindigul', 'dfdfdfd', 'coimbatore', '75843975349574', 'fsdfsdf', 'address', 'pin', 'contactname', 'email', 'department', 'fax', 'congaddress', 'conpin', 'congername', 'congemail', 'congerdepartment', 'congerfax', 'contain', 'declaredvalued', 'invoiceno', '2018-09-04', 'remarks', 'Actual Weight', '0', 'Blue Bundle', '0', '10', '100', 1000, 0, 0, 0, 0, 1000, 1000),
(100, 'document', '95864367398', 'coimbatore', 'dfdfdfd', 'coimbatore', '75843975349574', 'fsdfsdf', 'address', 'pin', 'contactname', 'email', 'department', 'fax', 'congaddress', 'conpin', 'congername', 'congemail', 'congerdepartment', 'congerfax', 'contain', 'declaredvalued', 'invoiceno', '2018-09-28', 'remarks', 'Actual Weight', '0', 'Blue Bundle', '0', '50', '10', 500, 5, 5, 5, 5, 500, 520);

-- --------------------------------------------------------

--
-- Table structure for table `receiver`
--

CREATE TABLE `receiver` (
  `id` int(11) NOT NULL,
  `serial` varchar(100) NOT NULL,
  `invoicedob` varchar(100) NOT NULL,
  `consignername` varchar(100) NOT NULL,
  `congname` varchar(100) NOT NULL,
  `destination` varchar(100) NOT NULL,
  `branch` varchar(100) NOT NULL,
  `typeofpacking` varchar(100) NOT NULL,
  `pkgs` varchar(100) NOT NULL,
  `weight` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `freight` varchar(100) NOT NULL,
  `grandtotal` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receiver`
--

INSERT INTO `receiver` (`id`, `serial`, `invoicedob`, `consignername`, `congname`, `destination`, `branch`, `typeofpacking`, `pkgs`, `weight`, `amount`, `freight`, `grandtotal`) VALUES
(9, '90', '2018-09-07', 'gdfs', 'fgfdgd', 'bhavani', 'dindigul', 'BAG', '5', '0', '6', '30', '30'),
(11, '80', '2018-09-05', 'thala', 'thala', 'bhavani', 'bhavani', 'Blue Bundle', '6', '0', '6', '36', '36'),
(12, '90', '2018-09-07', 'gdfs', 'fgfdgd', 'bhavani', 'dindigul', 'BAG', '5', '0', '6', '30', '30'),
(13, '94', '2018-09-07', 'guna', 'thala', 'ellampillai', 'bhavani', 'Blue Bundle', '0', '60', '6', '360', '372'),
(14, '94', '2018-09-07', 'guna', 'thala', 'ellampillai', 'bhavani', 'Blue Bundle', '0', '60', '6', '360', '372'),
(15, '97', '2018-09-13', 'thala', 'sankar', 'coimbatore', 'select', 'Bandal', '56', '0', '50', '2800', '2800');

-- --------------------------------------------------------

--
-- Table structure for table `topay`
--

CREATE TABLE `topay` (
  `id` int(11) NOT NULL,
  `document` varchar(100) NOT NULL,
  `mobileno` varchar(100) NOT NULL,
  `destination` varchar(100) NOT NULL,
  `consignername` varchar(100) NOT NULL,
  `branch` varchar(100) NOT NULL,
  `tomobileno` varchar(100) NOT NULL,
  `congname` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `pin` varchar(100) NOT NULL,
  `contactname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `congaddress` varchar(100) NOT NULL,
  `conpin` varchar(100) NOT NULL,
  `congername` varchar(100) NOT NULL,
  `congemail` varchar(100) NOT NULL,
  `congerdepartment` varchar(100) NOT NULL,
  `congerfax` varchar(100) NOT NULL,
  `contain` varchar(100) NOT NULL,
  `declaredvalued` varchar(100) NOT NULL,
  `invoiceno` varchar(100) NOT NULL,
  `invoicedob` varchar(100) NOT NULL,
  `remarks` varchar(100) NOT NULL,
  `package` varchar(100) NOT NULL,
  `pkgs` varchar(100) NOT NULL,
  `typeofpacking` varchar(100) NOT NULL,
  `ewaybill` varchar(100) NOT NULL,
  `weight` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `freight` double NOT NULL,
  `loading` double NOT NULL,
  `doorpickup` double NOT NULL,
  `doordelivery` double NOT NULL,
  `extra` double NOT NULL,
  `total` double NOT NULL,
  `grandtotal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `topay`
--

INSERT INTO `topay` (`id`, `document`, `mobileno`, `destination`, `consignername`, `branch`, `tomobileno`, `congname`, `address`, `pin`, `contactname`, `email`, `department`, `fax`, `congaddress`, `conpin`, `congername`, `congemail`, `congerdepartment`, `congerfax`, `contain`, `declaredvalued`, `invoiceno`, `invoicedob`, `remarks`, `package`, `pkgs`, `typeofpacking`, `ewaybill`, `weight`, `amount`, `freight`, `loading`, `doorpickup`, `doordelivery`, `extra`, `total`, `grandtotal`) VALUES
(3, 'nodocument', '1234456676', 'coimbatore', 'gdfs', 'india', '56366', 'fgfdgd', '', '', 'kurumberi', '', '', '', '', '', '', '', '', '', '', '', '1234', '2018-09-03', 'good', '', '5', 'super', '', '', '500', 0, 0, 0, 0, 0, 0, 500),
(5, 'nodocument', '9876543210', 'coimbatore', 'gdfs', 'india', '5634465', 'fgfdgd', 'fdfsdf', '12455', 'kurumberi', 'guna1081994@gmail.com', 'gfds', 'good', 'dfdsf', '12455', 'gunasekaran', 'sekar@gmail.com', 'mca', 'good', '', '', 'fgffdsg', '2018-08-28', 'fdgds', '', 'fdfda', 'dfdsaf', '', '', 'dfdsf', 0, 0, 0, 0, 0, 0, 67),
(7, 'document', '9876543210', 'dindiguls', 'gdfs', 'indias', '5634465', 'thalas', 'hfgs', '345345', 'dfsdfgds', 'guna@gmail.com', 'mca', 'good', 'fsdgsfd', '12455', 'kurumberi', 'sekar@gmail.com', 'dffad', 'good', 'dfdfdf', 'dfdfdf', '12343', '2018-08-01', 'verygoods', 'Actual Weight', '3', 'super', '0', '67', '330', 60, 0, 0, 0, 0, 60, 602),
(8, 'nodocument', '9876543210', 'dindigul', 'gdfs', 'india', '5634465', 'fgfdgd', 'dafdsf', '12455', 'dfsdfgd', 'guna1081994@gmail.com', 'mca', 'good', 'dsfdsf', '12455', 'gunasekaran', 'guna@gmail.com', 'mca', 'good', 'dfdfdf', 'gwgsf', '12345', '2018-08-13', 'verygood', 'Actual Weight', '5', '307', '0', '40kg', '5', 25, 0, 0, 0, 0, 25, 25),
(9, 'nodocument', '1234456676', 'coimbatore', 'thala', 'india', '56366', 'thala', 'fsdfgf', '12455', 'dfsdfgd', 'guna@gmail.com', 'mca', 'good', 'sdfg', '535', 'kurumberi', 'sekar@gmail.com', 'dffad', 'good', 'good', 'dfdfdf', '1234', '2018-08-06', 'verygood', 'Actual Weight', '5', '367', '56', '40kg', '50', 250, 0, 0, 0, 0, 250, 250),
(10, 'document', '9876543210', 'dindigul', 'thala', 'india', '5634465', 'thala', 'gunasekaran', '453345', 'kurumberi', 'me@gmail.com', 'gfds', 'good', 'gunasekaran', '12455', 'kurumberi', 'guna@gmail.com', 'mca', 'good', 'good', 'laptop', '1234', '2018-07-30', 'verygood', 'Actual Weight', '6', '367', '0', '10kg', '6', 36, 0, 0, 0, 0, 36, 36),
(11, 'document', '1234456676', 'coimbatore', 'thala', 'india', '5634465', 'thala', 'dfd', '345345', 'dfsdfgd', 'guna1081994@gmail.com', 'mca', 'good', 'dfdf', '56345', 'kurumberi', 'sekar@gmail.com', 'mca', 'good', 'good', 'laptop', '1234', '2018-07-30', 'good', 'Actual Weight', '7', '559', '0', '40kg', '7', 49, 0, 0, 0, 0, 49, 49);

-- --------------------------------------------------------

--
-- Table structure for table `userlogin`
--

CREATE TABLE `userlogin` (
  `id` int(11) NOT NULL,
  `username` varchar(60) NOT NULL,
  `pwd` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userlogin`
--

INSERT INTO `userlogin` (`id`, `username`, `pwd`) VALUES
(1, 'parcel', 'service'),
(2, 'guna', '1234');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dispatch`
--
ALTER TABLE `dispatch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manual`
--
ALTER TABLE `manual`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ones`
--
ALTER TABLE `ones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `padidetails`
--
ALTER TABLE `padidetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paid`
--
ALTER TABLE `paid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receiver`
--
ALTER TABLE `receiver`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topay`
--
ALTER TABLE `topay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userlogin`
--
ALTER TABLE `userlogin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dispatch`
--
ALTER TABLE `dispatch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `manual`
--
ALTER TABLE `manual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ones`
--
ALTER TABLE `ones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `padidetails`
--
ALTER TABLE `padidetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `paid`
--
ALTER TABLE `paid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `receiver`
--
ALTER TABLE `receiver`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `topay`
--
ALTER TABLE `topay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `userlogin`
--
ALTER TABLE `userlogin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
